# Acknowledgements

The adacraft editor is based on:
> * [Scratch](https://scratch.mit.edu/), developed by MIT
> * [TurboWarp](https://turbowarp.org/), a Scratch Mod developed by
>   [GarboMuffin](https://scratch.mit.edu/users/GarboMuffin/) and other
>   [contributors](https://turbowarp.org/credits.html).
> * [ScratchAddons](https://scratchaddons.com/) which improve the creative
>   experience of Scratch and friends, and are created and maintained by lots of
>   [contributors](https://scratchaddons.com/contributors/).

Contributors to adacraft extensions:
> * [Vittascience](https://vittascience.com/) develops a new extensions (like
>   one for microbit).
> * The [CloudLink](https://github.com/MikeDev101/cloudlink) extension is
>   developped by [MikeDEV](https://scratch.mit.edu/users/MikeDEV/).
> * [石原淳也 (Junya Ishihara)](https://twitter.com/jishiha) develops
>   Posenet2Scratch, ML2scratch, and TM2Scratch extensions.
> * Ada vision extension uses [ml5.js](https://ml5js.org/).
> * There is an extension that is based on [p5.js](https://p5js.org/).
> * The [Croquet](https://croquet.io/) extension is developed [よしき (Yoshiki
>   Ohshima)](https://twitter.com/yoshikiohshima) from the Croquet team.
