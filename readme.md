Welcome to the **adacraft** project basecamp on GitLab.

adacraft extends [Scratch](https://scratch.mit.edu/) with new tools to open up
new creative possibilities. With it, you can create interactive apps,
animations, generative art, quizzes, explorable explanations, music, games, etc.
Just like with Scratch.


> **quick links**: source code for the adacraft
> [website](https://gitlab.com/adacraft/portal/website) and for the adacraft
> [editor](https://gitlab.com/adacraft/scratch-mod) (which is a Scratch mod).

# The project

Developing adacraft is not only about developing a new Scratch mod, it is about
making block programming approachable for a broad audience of artists, creative
coders, students, etc. and to open up creative possibilities by adding new
features, blocks or extensions.

We are a collective of, and in solidarity with, people from every gender
identity and expression, sexual orientation, race, ethnicity, language,
neuro-type, size, ability, class, religion, culture, subculture, political
opinion, age, skill level, occupation, and background. We acknowledge that not
everyone has the time, financial means, or capacity to actively participate, but
we recognize and encourage involvement of all kinds. We facilitate and foster
access and empowerment. We are all learners.

See our [Collective Statemend](collective.md).

You are welcome to [contribute](contributing.md) to adacraft project.

# Source code

The main projects on this GitLab group are:
* The project [Portal /
  Website](https://gitlab.com/adacraft/portal/website) contains the source code
  for the main adacraft website ([www.adacraft.org](https://www.adacraft.org/)).
* The subgroup [Scratch mod](https://gitlab.com/adacraft/scratch-mod) the source
  code for the Scratch mod developed for adacraft (which is a fork of
  [TuboWarp](https://turbowarp.org/)).


The adacraft project is based on other softwares and contributions. See [acknowledgements](acknowledgements.md).

